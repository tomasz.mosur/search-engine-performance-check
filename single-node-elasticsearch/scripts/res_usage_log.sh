#!/bin/bash
log_file=$1

while true; do
    stats=$(docker stats --all --no-stream --format "table {{.CPUPerc}}\t{{.MemUsage}}\t{{.MemPerc}}" elasticsearch)
    echo -e "$(date +"%T")\n $stats\n" >>$log_file
done
